#!/usr/bin/python
#
# QT_NO_GLIB=1 python runTests.py ./analitzatest testTrivialEvaluate
#

import sys
import subprocess

executable = sys.argv[1]
test = sys.argv[2]

data = str(subprocess.check_output([executable, test+':'])).split('\\n')[5:-4]

for i in data:
  out = str(subprocess.check_output(["valgrind", "--leak-check=full", executable, test+':'+i], stderr=subprocess.STDOUT))
  
  if out.find('no leaks are possible')<0:
    print("Leaky test!", i)
  else:
    print("Clean test :)", i)



BASEDIR=~
mkdir -p $BASEDIR
cd $BASEDIR

ERRORSFILE=$HOME/debug/errors
echo start > $ERRORSFILE

source ~/devel/kdescripts/devmacros.sh
modules=$*

#token=`qdbus org.freedesktop.PowerManagement /org/freedesktop/PowerManagement org.freedesktop.PowerManagement.Inhibit.Inhibit updatekde "the program is compiling"`
#trap "qdbus org.freedesktop.PowerManagement /org/freedesktop/PowerManagement org.freedesktop.PowerManagement.Inhibit.UnInhibit $token" EXIT

# echo "Starting... $PPID"

failedBuilds=""
for i in $modules
do
    result="??"
    echo -n "$i. "

    if [[ $NUKEBUILDDIR == 1 ]]
    then
        rm -rf "$BASEDIR/build-$i"
    fi

    mkdir -p $BASEDIR/build-$i
    cd $BASEDIR/build-$i

    found=0
    if [ -e $BASEDIR/build-$i/CMakeCache.txt ]
    then
        if [ -e $BASEDIR/build-$i/Makefile ]
        then
            echo -n "make.. "
            nice make install -j3 2>> $ERRORSFILE 1>> $ERRORSFILE
            result=$?
            found=2
        elif [ -e $BASEDIR/build-$i/build.ninja ]
        then
            echo -n "ninja.. "
            nice ninja install 2>> $ERRORSFILE 1>> $ERRORSFILE
            result=$?
            found=3
        fi
    elif [ -e $BASEDIR/$i/Makefile ]
    then
        echo -n "rudimentary make.. "
        cd $BASEDIR/$i
        make -j3 2>> $ERRORSFILE 1>> $ERRORSFILE
        result=$?
        found=4
    fi

    if [[ $found == 0 ]]
    then
        echo -n "configuring, building. "
        cmakekf5 $BASEDIR/$i 2>> $ERRORSFILE 1>> $ERRORSFILE
        nice ninja install 2>> $ERRORSFILE 1>> $ERRORSFILE
        result=$?
    fi

    if [[ $result == 0 ]]
    then
        echo "done."
    else
        echo -e "\033[01;31merror: $result\033[0m"
        failedBuilds="$failedBuilds $i"
    fi
    sync &
done

test -n "$failedBuilds" && echo -e "\n\033[01;31merrors:\033[0m $failedBuilds"

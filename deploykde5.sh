source /home/apol/devel/kdescripts/devmacros.sh

#export KDEWM=openbox

export KF5=/home/apol/devel/kde5

export XDG_DATA_DIRS=$KF5/share:/usr/share
export XDG_CONFIG_DIRS=$KF5/etc/xdg:/etc/xdg
export PATH=~apol/devel/kdescripts/:$KF5/bin:$PATH
export QT_PLUGIN_PATH=$KF5/lib64/plugins #:$QT_PLUGIN_PATH:$QTDIR/plugins:$KF5/lib/x86_64-linux-gnu/plugins:$KF5/lib/plugins
export QML2_IMPORT_PATH=$KF5/lib64/qml:$QTDIR/qml
# export GST_PLUGIN_SYSTEM_PATH=$KF5/gstreamer-0.10/:/usr/lib/gstreamer-0.10/
export GST_PLUGIN_SYSTEM_PATH=$KF5/lib64/gstreamer-1.0/:/usr/lib/gstreamer-1.0

export KDE_FULL_SESSION=true #otherwise we don't get the QPA
export KDE_SESSION_VERSION=5
export PKG_CONFIG_PATH=$KF5/lib64/pkgconfig:$KF5/lib/pkgconfig
export LD_LIBRARY_PATH=$KF5/lib:$KF5/lib64

QT_MESSAGE_PATTERN="`echo -e "\033[32m%{if-category}\033[32m %{category}:%{endif} %{if-debug}\033[34m%{function}%{endif}%{if-warning}\033[31m%{backtrace depth=3}%{endif}%{if-critical}\033[31m%{backtrace depth=3}%{endif}%{if-fatal}\033[31m%{backtrace depth=3}%{endif}\033[0m %{message}"`"

export CC="clang"
export CXX="clang++"

export GTK_USE_PORTAL=1


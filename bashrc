
# Check for an interactive session
[ -z "$PS1" ] && return

if [ -f /etc/bash_completion ]; then
    . /etc/bash_completion
fi


alias ls='ls --color=auto'
#PS1='[\u@\h \W]\$ '
PS1="\[\033[02;32m\]\u@\h:\[\033[00m\]\w\$ "


export SVN_EDITOR=vim

alias mancmake="cmake --help-command"
source deploykde4.sh
export EDITOR=vim

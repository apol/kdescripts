source ~/devel/kdescripts/deploykde5.sh

extrapackages="extra-cmake-modules qca libdbusmenu-qt grantlee icemon PackageKit-Qt GammaRay poppler keepassx clazy signond signon-ui libaccounts-qt accounts-qml-module hotspot snapd-glib gpgme" #appstream

if [[ ! -z $@ ]]; then
    packages=$@
else
    packages="kf5umbrella plasma-desktop plasma-integration kate plasma-nm kdeplasma-addons discover kamoso kdevelop dolphin konsole yakuake kdeconnect-kde kalgebra gwenview kscreen kde-gtk-config polkit-kde-agent-1 kapptemplate kinfocenter massif-visualizer kcachegrind signon-kwallet-extension kaccounts-providers plasma-pa skanlite spectacle ark heaptrack ktorrent kdebugsettings kdepim-runtime akregator xdg-desktop-portal-kde plasma-browser-integration kwayland-integration drkonqi bluedevil kirigami-gallery kde-cli-tools plasma-thunderbolt okular plasma-sdk"
fi

all_packages=`python3 ~/devel/playground/ci-tooling/helpers/list-dependencies.py --platform SUSEQt5.9 --withProject $packages`
if [[ $? -ne 0 ]]
then
    echo "error finding dependencies: $all_output"
    exit 21
fi

# include extra packages if we aren't trying to build something specific
if [[ -z $@ ]]; then
    all_packages="$extrapackages $all_packages"
fi

blacklist="gpgme"
modules=""
BASEDIR=~
for a in $all_packages
do
    correct=0
    if [[ "$blacklist" =~ "$a" ]]
    then
        continue
    fi

    path="devel/frameworks/$a"
    if [ ! -d $BASEDIR/$path ]
    then
        echo "cloning $a..."
        pushd ~/devel/frameworks/ >/dev/null
        git clone kde:$a 2>/dev/null
        correct=$?
        popd >/dev/null
    fi

    if [ $correct -gt 0 ]
    then
        echo "skipping $a $z"
    else
        modules="$modules $path"
    fi
done

updatedirs $modules

nice ~apol/devel/kdescripts/updatekf5.sh $modules

#sudo cp -r /home/apol/devel/kde5/share/dbus-1/* /usr/share/dbus-1/

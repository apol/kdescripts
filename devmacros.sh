function cdp {
	cd ~/devel/frameworks/$@
}

function cdb {
	cd ~/build-devel/frameworks/$@
}

function runicecream {
	PATH=/usr/lib/icecream/libexec/icecc/bin/:$PATH $@
}

function cmakekdeninja {
        cmake $@ -DCMAKE_PREFIX_PATH=$KDEDIR -DCMAKE_INSTALL_PREFIX=$KDEDIR -DCMAKE_BUILD_TYPE=debugfull -DQT_QMAKE_EXECUTABLE=/usr/bin/qmake-qt4 -GNinja && ninja install
}

function cmakekf5 {
	cmake $@ -DECM_DIR=/home/apol/devel/kde5/share/ECM/cmake -DCMAKE_INSTALL_PREFIX=$KF5 -DCMAKE_PREFIX_PATH=$KF5 -DCMAKE_BUILD_TYPE=debug -GNinja
}

function cmakemakekf5 {
	cmake $@ -DECM_DIR=/home/apol/devel/kde5/share/ECM/cmake -DCMAKE_INSTALL_PREFIX=$KF5 -DCMAKE_PREFIX_PATH=$KF5 -DCMAKE_BUILD_TYPE=debug
}

function cmakekde {
        # to enable tests, add -DKDE4_BUILD_TESTS=TRUE to the next line.
        # you can also change "debugfull" to "debug" to save disk space.
        cmake $@ -DCMAKE_INSTALL_PREFIX=$KDEDIR -DCMAKE_BUILD_TYPE=debugfull -DQT_QMAKE_EXECUTABLE=/usr/bin/qmake-qt4 && \
        make install -j3;
}

function makeandrun {
	if [ -z "$DISPLAY" ]
	then
		DISPLAY=:1
	fi
	exec=$1
	nom=$2
	shift; shift;
	make install -j5 > /dev/null && $exec $@ 2> ~/debug/$exec-$nom
}

function runsilently
{
	if [ -z "$DISPLAY" ]
	then
		DISPLAY=:1
	fi

	EXEC=$1
	shift
	$EXEC $@ 2> /dev/null > /dev/null
}

alias cleanDir="find . -name \"*~\" | xargs rm"
alias open="kde-open 2> /dev/null > /dev/null "
alias git-localchanges="git log origin/master..master"
alias mymad='GCCWRAPPER_PATHMANGLE=/usr ~/QtSDK/Madde/bin/mad -t harmattan-platform-api'
alias gpra='git pull --rebase --autostash'

function findicon
{
	find $KDEDIR/share/icons/oxygen/32x32/ $KDEDIR/share/icons/hicolor/32x32/ | grep $@ | sort | uniq
}

#funcion svg2png
#{
#	inkscape -z -D $1 --export-width=$2 --export-png=$3
#}

#alias nepomukcmd="sopranocmd --socket `kde4-config --path socket`nepomuk-socket --model main --nrl"

